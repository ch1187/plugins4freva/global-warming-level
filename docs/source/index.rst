.. gwl-loader documentation master file, created by
   sphinx-quickstart on Wed Sep 25 14:59:56 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Global Warming Levels
=====================

This is a simple python based library that allows for calculation of global
warming levels. The library also comes with a command line interface.

The following will show two simple examples of how to use this library.


Installation
============

We have already made an instance available within the freva framework of regiklim.
All you'll have to do is loading the regiklim-ces module:

.. code-block:: bash

    module load clint regiklim-ces/2022.03

If you wish to install the library yourself you can do so by using `pip`:

.. code-block:: bash

    pip install git+https://gitlab.dkrz.de/ch1187/plugins4freva/global-warming-level.git


**Note:** This will not install `freva` and you wont be able to use freva as a
search backend. If you want to make use of freva you have to clone the repository
and issue the following `pip` command:

.. code-block:: bash

    git clone https://gitlab.dkrz.de/ch1187/plugins4freva/global-warming-level.git
    cd global-warming-level
    pip install .[freva]



Usage with examples
-------------------

.. toctree::
   :maxdepth: 3

   GWLLoader-cli.ipynb
   GWLLoader-python.ipynb


API Reference
-------------

.. toctree::
   :maxdepth: 3

   modules

.. seealso::
    - `CMIP6 via ESGF <https://esgf-data.dkrz.de/search/cmip6-dkrz/>`_
    - `Freva <https://gitlab.dkrz.de/freva/evaluation_system>`_
    - `Intake-ESM <https://intake-esm.readthedocs.io/en/stable/>`_
    - `Xarray <http://xarray.pydata.org/en/stable/>`_




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
