GWLLoader class
===============

.. toctree::
   :maxdepth: 3

.. automodule:: gwl_loader._gwl
   :members:
   :undoc-members:
   :show-inheritance:
