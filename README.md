# Global Warming Level Loader

A simple library to calculate global warming level.

This library offers a python module and a command line interface to calculate
global warming levels for cmip5 and cmip6 models.

## Installation:
We have already a instance made available within the freva framework of regiklim.
All you'll have to do is loading the regiklim-ces module:

```bash
module load clint regiklim-ces/2022.03
```

If you which to install the library yourself you can do use `pip`:

```bash
pip install git+https://gitlab.dkrz.de/ch1187/plugins4freva/global-warming-level.git
```

**Note:** This will not install `freva` and you wont be able to use freva as a
search backend. If you want to make use of freva you'll have to clone the repository
and issues the following `pip` command:

```bash
git clone https://gitlab.dkrz.de/ch1187/plugins4freva/global-warming-level.git
cd global-warming-level
pip install .[freva]
```


## Basic usage:

Please see the [Documentation page](https://ch1187.gitlab-pages.dkrz.de/plugins4freva/global-warming-level/index.html)
for usage instructions.
