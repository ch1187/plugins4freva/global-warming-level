"""GWL Loader library."""
from __future__ import annotations

import importlib

__version__ = "2023.01"
__all__ = ["GWLLoader"]


def __getattr__(name: str) -> None:
    try:
        return getattr(importlib.import_module("._gwl", __name__), name)
    except AttributeError:
        pass
    raise AttributeError(f"module {__name__!r} has no attribute {name!r}")
