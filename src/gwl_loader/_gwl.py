"""Module that calculates global warming levels."""
from __future__ import annotations
from contextlib import contextmanager
from functools import cached_property
import sys
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Any, Iterator, Hashable, Optional, Union

from cftime import datetime as dt
import pandas as pd
import numpy as np
import xarray as xr

__all__ = ["GWLLoader"]


@contextmanager
def _silence() -> Iterator[None]:

    with NamedTemporaryFile() as tf:
        try:
            stdout = sys.stdout
            f = open(tf.name, "w")
            sys.stdout = f
            yield
        finally:
            sys.stdout = stdout
            try:
                f.close()
            except AttributeError:
                pass


class GWLLoader:
    """Data loader class that creates global averages of given models.

    Parameters
    ----------

    model_name: str
        The name of the model that is considered
    window_length: int, default: 30
        Number of years used to calculate the rolling average
    temperature_variable: str, default: tas
        Variable name of the near surface temperature variable
    baseline_period: tuple[int, int], default: (1961, 1990)
        Start and end years of the reference period.
    **experiments:
        Mapping holding the model data by experiment name


    Properties
    ----------

    data_array : xarray.DataArray
        xaray DataArray representation of the global averaged dataset
    data_frame: pandas.DataFrame
        pandas DataFrame representation of the global averaged dataset
    """

    _ref_delta: float = 0.42
    """Global climate change signal (18881-1910) based on HadCRUT5"""
    _period: slice = slice(1865, 2086)
    """First and last years of the historical and projection periods."""
    temperature_variable: str = "tas"
    """Name of the temperature variable that is considered."""
    window_length: int = 30
    """The years used to calculate the rolling avg."""
    baseline_period: tuple[int, int] = (1961, 1990)
    """String representation of years defining the reference period."""
    open_kwargs: dict[str, Union[str, bool, int]] = dict(
        parallel=False,
        coords="minimal",
        data_vars="minimal",
        combine="by_coords",
        use_cftime=True,
    )

    @staticmethod
    def _drop_all_bounds(dset: xr.Dataset) -> xr.Dataset:
        """Drop all bounds variables if present."""
        drop_vars = []
        for vname in map(str, dset.data_vars):
            if "bounds" in vname or "bnds" in vname:
                drop_vars.append(vname)
        return dset.drop(drop_vars)

    @staticmethod
    def _get_dim_name(dset: xr.Dataset, dim_name: str) -> str:
        """Get the name of a dimension."""
        for dim in map(str, dset.dims.keys()):
            if dim_name in dim:
                return dim
        raise ValueError("Couldn't find a latitude coordinate")

    @classmethod
    def weighted_mean(cls, dataset: xr.Dataset) -> xr.Dataset:
        """Calculate a weighted area average of a input dataset.

        Parameters
        ----------
        dataset: xarray.Dataset
            Input dataset that should be averaged

        Returns
        -------
        xarray.Dataset
            Weighted field mean of the dataset

        """
        lat_name = cls._get_dim_name(cls._drop_all_bounds(dataset), "lat")
        lon_name = cls._get_dim_name(cls._drop_all_bounds(dataset), "lon")
        weight = np.cos(np.deg2rad(dataset[lat_name].values))
        if len(dataset[lat_name].shape) == 1:
            _, weight = np.meshgrid(np.ones_like(dataset[lon_name]), weight)
        x_weight = xr.DataArray(
            weight,
            dims=(lat_name, lon_name),
            coords={lon_name: dataset[lon_name], lat_name: dataset[lat_name]},
        )
        return (x_weight * cls._drop_all_bounds(dataset)).sum(
            dim=(lat_name, lon_name), keep_attrs=True
        ) / weight.sum()

    def __init__(
        self,
        model_name: str,
        window_length: Optional[int] = None,
        baseline_period: Optional[tuple[int, int]] = None,
        temperature_variable: Optional[str] = None,
        **experiments: xr.Dataset,
    ):
        """Instantiate GWLLoader class."""
        self.window_length = window_length or self.window_length
        self.baseline_period = baseline_period or self.baseline_period
        self.temperature_variable = (
            temperature_variable or self.temperature_variable
        )
        self.model_name = model_name
        _experiments = xr.DataArray(
            list(experiments.keys()),
            dims="experiment_id",
            name="experiment_id",
            coords={"experiment_id": list(experiments.keys())},
        )
        model = xr.DataArray(
            [model_name],
            dims="model",
            name="model",
            coords={"model": [model_name]},
        )
        dsets = []
        for dset in experiments.values():
            attrs = {v: dset[v].attrs for v in dset.data_vars}
            global_attrs = dset.attrs
            dset.coords["time"] = [
                dt(date.year, date.month, 15) for date in dset.time.values
            ]
            dsets.append(self.weighted_mean(dset).groupby("time.year").mean())
        _dataset = xr.concat(
            [xr.concat(dsets, join="outer", dim=_experiments)],
            join="outer",
            dim=model,
        )
        global_attrs.update(attrs[self.temperature_variable])
        try:
            _dataset = _dataset.squeeze("member_id", drop=True)
        except (KeyError, ValueError):
            pass
        try:
            self._dataset = self._prep_dataset(
                _dataset[self.temperature_variable], global_attrs
            )
        except KeyError:
            self._dataset = self._prep_dataset(_dataset, global_attrs)

    @staticmethod
    def _get_overlapping_years(
        dset: xr.DataArray,
    ) -> Optional[tuple[int, int]]:
        """Get overlap years in both experiments."""
        years, exps = {}, []
        for exp in dset.experiment_id.values:
            years[exp] = (
                dset.sel(experiment_id=exp).dropna(dim="year").year.values
            )
            exps.append(exp)
        num_years = [y for y in years[exps[0]] if y in years[exps[1]]]
        try:
            return int(num_years[0]), int(num_years[-1])
        except IndexError:
            return None

    def _prep_dataset(
        self, dset: xr.DataArray, attrs: dict[Hashable, Any]
    ) -> xr.DataArray:
        """Prepare the datasets for processing."""
        hist_slice = slice(*self._get_last_hist_years(dset))
        overlap = self._get_overlapping_years(dset)
        for exp in dset.experiment_id:
            dset.loc[
                {"year": hist_slice, "experiment_id": [exp.values]}
            ] = dset.sel(year=hist_slice, experiment_id=["historical"]).data
        if overlap:
            dset.loc[
                {"year": slice(*overlap), "experiment_id": "historical"}
            ] = np.nan
        dset.attrs = attrs
        return dset.persist()

    def _get_last_hist_years(self, dset: xr.DataArray) -> tuple[int, int]:
        """Get the years that are used as an overlap period for the rolling mean."""
        years = (
            dset.sel(experiment_id="historical").dropna(dim="year").year.values
        )
        return (years[-1] - self.window_length), years[-1]

    @cached_property
    def data_array(self) -> xr.DataArray:
        """xarray.DataArray representation of the global rolling avg.

        Returns
        -------
        xarray.DataArray
            Data array representation of the global rolling avg.
        """
        rolling_mean = (
            (self._dataset - self._reference)
            .rolling(year=self.window_length, center=True)
            .mean()
        )
        # since same value exits for year 2000 in both historical and ssp370
        # time series, we fill in nan
        rolling_mean = rolling_mean.mean(dim="experiment_id", keep_attrs=True)
        rolling_mean = rolling_mean.sel(year=self._period) + self._ref_delta
        rolling_mean.attrs = self._dataset.attrs
        return rolling_mean

    @cached_property
    def data_frame(self) -> pd.DataFrame:
        """Get a pandas DataFrame representation of the global rolling avg.

        Returns
        -------
        pandas.DataFrame
            Data frame representation of the global rolling avg.
        """
        dataframe = self.data_array.isel(model=0).to_dataframe()[
            self.temperature_variable
        ]
        return pd.DataFrame({self.model_name: dataframe})

    def get_global_warming_year(
        self, level: Union[int, float] = 2
    ) -> Optional[tuple[int, int]]:
        """Get the year a given global warming level is reached.

        Parameters
        ----------
        level: float, int
               Given global warming level

        Returns
        -------
        years: tuple[int, int] | None
              First and last years of the time window when the global warming
              level is reached, returns None if level isn't reached.
        """
        dframe = self.data_frame
        try:
            c_year = self.data_frame.loc[
                dframe[self.model_name] >= level
            ].index[0]
        except IndexError:
            return None
        return (
            c_year - self.window_length // 2,
            c_year + self.window_length // 2,
        )

    @cached_property
    def _reference(self) -> xr.Dataset:
        """Calculate the reference dataset."""
        return self._dataset.sel(
            year=slice(*self.baseline_period), experiment_id="historical"
        ).mean(dim="year")

    @classmethod
    def load_data_with_intake(
        cls,
        model: str,
        experiment: str = "ssp370",
        project: str = "cmip6",
        temperature_variable: str = "tas",
        baseline_period: tuple[int, int] = (1961, 1990),
        window_length: int = 30,
        **kwargs: str,
    ):
        """Read data using intake-esm catalog search.

        This method uses an intake catalog to search for data.

        Parameters
        ----------
        model: str
            The name of the cmip6 model
        experiment: str
            The name of the experiment for which the global warming level is
            calculated.
        project: str
            Wether cmip6 or cmip5 data should be used.
        experiment: str
            The name of the experiment for which the global warming level is
            calculated.
        window_length: int, default: 30
            Number of years used to calculate the rolling average
        temperature_variable: str, default: tas
            Variable name of the near surface temperature variable
        baseline_period: tuple[int, int], default: (1961, 1990)
            Start and end years of the reference period.
        **kwargs: str
            Additional search keywords to refine the search.

        Returns
        -------
        GWLLoader
            Instance of the GWLLoader class

        Example
        -------

        ::

            from gwl_loader import GWLLoader
            Data = GWLLoader.load_data_with_intake("MPI-ESM1-2-HR", member_id='r1i1p1*')
            Data.get_global_warming_year(2.4)

        """
        import intake

        catalogs: dict[str, str] = dict(
            cmip6="/work/ik1017/Catalogs/mistral-cmip6.json",
            cmip5="/work/ik1017/Catalogs/mistral-cmip5.json",
        )
        """Path or url to the intake catalog definitions"""
        search_kwargs = dict(
            source_id=model,
            experiment_id=("historical", experiment),
            variable_id=temperature_variable,
            table_id="Amon",
            member_id=kwargs.get("ensemble", "r1i1p1f1"),
            activity_id=["CMIP", "ScenarioMIP"],
        )
        with _silence():
            catalog = intake.open_esm_datastore(catalogs[project])
            search_res = catalog.search(**search_kwargs)
            cdf_kwargs = {"use_cftime": True, "chunks": {"time": 6}}
            out_dict = dict()
            for key, dset in search_res.to_dataset_dict(
                cdf_kwargs=cdf_kwargs
            ).items():
                _, _, _, experiment, _, _ = key.split(".")
                out_dict[experiment] = dset
        return cls(
            model,
            baseline_period=baseline_period,
            temperature_variable=temperature_variable,
            window_length=window_length,
            **out_dict,
        )

    @classmethod
    def load_data_with_freva(
        cls,
        model: str,
        experiment: str = "ssp370",
        temperature_variable: str = "tas",
        baseline_period: tuple[int, int] = (1961, 1990),
        window_length: int = 30,
        **kwargs: str,
    ):
        """Read data using freva databrowser search.

        This method uses the freva databrowser to search for data.

        Parameters
        ----------
        model: str
            The name of the cmip6 model
        experiment: str
            The name of the experiment for which the global warming level is
            calculated.
        window_length: int, default: 30
            Number of years used to calculate the rolling average
        temperature_variable: str, default: tas
            Variable name of the near surface temperature variable
        baseline_period: tuple[int, int], default: (1961, 1990)
            Start and end years of the reference period.
        **kwargs: str
            Additional search keywords.

        Returns
        -------
        GWLLoader
            Instance of the GWLLoader class

        Example
        -------

        ::

            from gwl_loader import GWLLoader
            Data = GWLLoader.load_data_with_freva("MPI-ESM1-2-HR", ensemble='r1i1p1*')
            Data.get_global_warming_year(2.4)

        """
        import freva

        out_dict = {}
        search_kwargs = dict(
            time_frequency="mon",
            model=model,
            project=kwargs.get("project", "cmip6"),
            variable=temperature_variable,
        )
        search_kwargs.update(kwargs)
        for exp in ("historical", experiment):
            search_kwargs["experiment"] = exp
            files = [
                f
                for f in sorted(freva.databrowser(**search_kwargs))
                if Path(f).suffix == ".nc"
            ]
            out_dict[exp] = xr.open_mfdataset(files, **cls.open_kwargs)
        return cls(
            model,
            baseline_period=baseline_period,
            temperature_variable=temperature_variable,
            window_length=window_length,
            **out_dict,
        )
