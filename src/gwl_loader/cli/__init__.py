"""Command line interface for global warming level calculation."""
from __future__ import annotations
import argparse
import logging
import json
from pathlib import Path
import sys
import toml
from typing import cast, Optional, Union

import dask
import numpy as np
import pandas as pd
import xarray as xr

from .._gwl import GWLLoader

PROG_NAME = "gwl-loader"
logging.basicConfig(format="%(name)s - %(levelname)s - %(message)s", level=logging.INFO)
logger: logging.Logger = logging.getLogger(PROG_NAME)


class ArgParser:
    """Command line interface definition.

    Properties
    ----------
    kwargs: dict[str, Union[str, float, Path]]
            property holding all parsed keyword arguments.
    """

    kwargs: dict[str, Union[str, float, Path, int]] = {}
    verbose: int = 0

    def __init__(self) -> None:
        """Instantiate the CLI class."""

        self.parser = argparse.ArgumentParser(
            prog=PROG_NAME,
            description="Calculate global warming level.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        self.parser.add_argument("-v", "--verbose", action="count", default=0)
        self.subparsers = self.parser.add_subparsers(
            help="Available sub-commands:",
            required=True,
        )
        self._get_time_series_sub_cmd()
        self._get_year_sub_cmd()

    def _get_time_series_sub_cmd(self) -> None:
        """Add sub command for creating a time-series."""
        parser = self.subparsers.add_parser(
            "time-series",
            description="Save a time series a field mean to file.",
            help="Save a time series a field mean to file.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        self._add_cmip_args(parser)
        parser.add_argument(
            "--output",
            type=str,
            help="Set the output file",
            default=None,
        )
        parser.add_argument(
            "--file-type",
            "-f",
            help="Set the file type of the output file.",
            default="toml",
            choices=["json", "toml"],
        )
        parser.set_defaults(apply_func=_get_time_series)

    def _add_cmip_args(self, parser) -> None:
        """Add standard arguments defining the search setup."""
        parser.add_argument("model", type=str, help="The cmip model", nargs="*")
        parser.add_argument(
            "--project",
            type=str,
            help="Indicate whether cmip5 or cmip6 data should be used.",
            default="cmip6",
            choices=["cmip6", "cmip5"],
        )
        parser.add_argument(
            "--experiment",
            type=str,
            help="Set the climate change projection experiment.",
            default="ssp370",
        )
        parser.add_argument(
            "--ensemble",
            type=str,
            help="set the ensemble/member_id",
            default="r1i1p1f1",
        )
        parser.add_argument(
            "--temperature-variable",
            type=str,
            default=GWLLoader.temperature_variable,
            help="Variable name of the near surface temperature variable.",
        )
        parser.add_argument(
            "--baseline-period",
            type=int,
            nargs=2,
            default=GWLLoader.baseline_period,
            help="Start and end years of the reference period.",
        )
        parser.add_argument(
            "--window-length",
            type=int,
            default=GWLLoader.window_length,
            help="Number of years used to calculate the rolling average.",
        )

        parser.add_argument(
            "--json-file",
            type=Path,
            help=argparse.SUPPRESS,
            default=None,
        )
        parser.add_argument(
            "--method",
            type=str,
            help="Set the data search method (intake or freva)",
            default="intake",
            choices=("freva", "intake"),
        )

    def _get_year_sub_cmd(self) -> None:
        """Add sub parser to calculate the year of a global warming level."""

        parser = self.subparsers.add_parser(
            "get-year",
            description="Get the year a GWL is reached.",
            help="Get the year a GWL is reached.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        parser.add_argument(
            "level",
            type=float,
            help="The global warming level",
        )
        self._add_cmip_args(parser)
        parser.set_defaults(apply_func=_get_year)

    @property
    def log_level(self):
        """Get the log level."""
        return max(logging.ERROR - 10 * self.verbose, logging.DEBUG)

    def parse_args(self, argv: list[str]) -> argparse.Namespace:
        """Parse the arguments.

        Parameters
        ----------
        argv: list[str]
              List of command line arguments that is parsed.

        Returns
        -------
        argparse.Namespace
            parsed Namespace object.
        """
        args = self.parser.parse_args(argv)
        self.kwargs = {
            k: v for (k, v) in args._get_kwargs() if k not in ("apply_func", "verbose")
        }
        self.verbose = args.verbose
        logger.setLevel(self.log_level)
        return args


def _read_datasets_from_json_file(
    project: str, model: str, experiments: tuple[str, str], json_file: Path
) -> dict[str, xr.Dataset]:
    with json_file.open() as f:
        config = json.load(f)
    out = {}
    for exp in experiments:
        key = f"{project}.{model}.{exp}"
        out[exp] = xr.open_mfdataset(config[key], **GWLLoader.open_kwargs)
    return out


@dask.delayed
def _get_gwl(model: str, json_file: Optional[Path] = None, **kwargs):
    experiments: tuple[str, str] = "historical", cast(str, kwargs["experiment"])
    method: str = cast(str, kwargs.pop("method", "freva"))
    if json_file:
        args = kwargs["project"], model, experiments, json_file
        gwl = GWLLoader(
            model,
            window_length=cast(int, kwargs["window_length"]),
            baseline_period=cast(tuple[int, int], kwargs["baseline_period"]),
            temperature_variable=cast(str, kwargs["temperature_variable"]),
            **_read_datasets_from_json_file(*args),
        )
    elif method == "intake":
        gwl = GWLLoader.load_data_with_intake(model, **kwargs)
    else:
        gwl = GWLLoader.load_data_with_freva(model, **kwargs)
    return gwl.data_frame


def _get_time_series(
    models: list[str], json_file: Optional[Path] = None, **kwargs: str
) -> None:
    """Calculate a global field mean."""
    file_type = kwargs.pop("file_type")
    project = kwargs.get("project")
    ensemble = kwargs.get("ensemble")
    out_file = Path(kwargs.pop("output", "") or f"GWL_{project}.toml")
    out_file = out_file.with_suffix(f".{file_type}")
    futures = [_get_gwl(m, json_file, **kwargs) for m in models]
    results = dask.compute(*futures)
    try:
        with out_file.open() as f:
            if file_type == "toml":
                config = toml.load(f)
            else:
                config = json.load(f)
    except FileNotFoundError:
        config = {m: {ensemble: {"year": [], "gwl": []}} for m in models}
    for result in results:
        model = result.columns[0]
        values = result[model].values.tolist()
        idx = np.array(result.index, dtype=np.int32).tolist()
        config[model.lower()] = {ensemble: {"year": idx, "gwl": values}}
    with open(out_file, "w") as f:
        if file_type == "toml":
            toml.dump(config, f)
        else:
            json.dump(config, f, indent=4)


def _get_year(models: list[str], json_file: Optional[Path] = None, **kwargs) -> None:
    """Get the year a global warming level is reached."""
    level = kwargs.pop("level")
    win_l = kwargs["window_length"] // 2
    futures = [_get_gwl(m, json_file, **kwargs) for m in models]
    results = dask.compute(*futures)
    for result in results:
        if isinstance(result, pd.DataFrame):
            model = result.columns[0]
            try:
                c_year = result[model].loc[result[model] >= level].index[0]
                year = f"{c_year - win_l} - {c_year + win_l}"
            except IndexError:
                year = "did not reach"
        else:
            year = "IO Error"
        print(f"{model.lower()}: {year}")


def cli() -> None:
    """Method the creates the command line argument parser."""

    AP = ArgParser()
    args = AP.parse_args(sys.argv[1:])
    models = AP.kwargs.pop("model")
    AP.kwargs["baseline_period"] = tuple(map(int, AP.kwargs["baseline_period"]))
    args.apply_func(models, **AP.kwargs)


if __name__ == "__main__":
    cli()
