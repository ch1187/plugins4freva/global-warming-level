#!/usr/bin/env python

from pathlib import Path
import re
from setuptools import setup, find_packages


def read(*parts):
    return open(Path(".").joinpath(*parts)).read()


def find_version(*parts):
    vers_file = read(*parts)
    match = re.search(r'^__version__ = "(\d+.\d+)"', vers_file, re.M)
    if match is not None:
        return match.group(1)
    raise RuntimeError("Unable to find version string.")


def entry_point():
    return ["gwl-loader = gwl_loader.cli:cli"]


setup(
    name="gwl_loader",
    version=find_version("src", "gwl_loader", "__init__.py"),
    author="German Climate Computing Centre (DKRZ)",
    maintainer="Climate Informatics and Technology (CLINT)",
    description="Calculate global warming levels ",
    long_description=read("README.md"),
    long_description_content_type="text/markdown",
    license="BSD-3-Clause",
    packages=find_packages("src"),
    package_dir={"": "src"},
    install_requires=[
        "intake-esm",
        "dask",
        "numpy",
        "pandas",
        "toml",
        "xarray",
    ],
    extras_require={
        "freva": [
            "freva",
        ],
        "docs": [
            "sphinx",
            "nbsphinx",
            "numpydoc",
            "sphinx_rtd_theme",
            "recommonmark",
            "pandoc",
            "ipython",  # For nbsphinx syntax highlighting
            "sphinxcontrib_github_alt",
        ],
        "test": [
            "black",
            "pytest",
            "nbformat",
            "pytest-html",
            "pytest-env",
            "pytest-cov",
            "pep257",
            "nbval",
            "h5netcdf",
            "mock",
            "mypy",
            "requests_mock",
            "allure-pytest",
            "testpath",
            "types-toml",
            "types-mock",
            "types-requests",
        ],
    },
    entry_points={"console_scripts": entry_point()},
    python_requires=">=3.7",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Topic :: Scientific/Engineering :: Data Analysis",
        "Topic :: Scientific/Engineering :: Earth Sciences",
    ],
)
